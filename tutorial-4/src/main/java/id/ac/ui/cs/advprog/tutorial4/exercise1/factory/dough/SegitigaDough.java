package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SegitigaDough implements Dough {
    public String toString() {
        return "Indonesian triangle style dough";
    }
}
