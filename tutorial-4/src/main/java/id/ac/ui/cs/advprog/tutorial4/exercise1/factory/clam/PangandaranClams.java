package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class PangandaranClams implements Clams {

    public String toString() {
        return "Fresh Clams from Pangandaran";
    }
}
