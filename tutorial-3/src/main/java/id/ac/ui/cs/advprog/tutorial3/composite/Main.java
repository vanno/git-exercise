package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee(new Ceo("Adit", 500000000));
        company.addEmployee(new Cto("Vanno", 1000000));
        company.addEmployee(new BackendProgrammer("Dayat", 500000));
        company.addEmployee(new FrontendProgrammer("Dipo", 30000));

        System.out.println("List of employees: ");
        for (Employees employees : company.getAllEmployees()) {
            System.out.println(employees.getName() + " as " + employees.getRole());
        }
        System.out.println("Total company net worth: " + company.getNetSalaries());
    }
}