package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    public static void main(String[] args) {
        Food food = null;
        food = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        food = FillingDecorator.CHILI_SAUCE.addFillingToBread(food);
        food = FillingDecorator.TOMATO.addFillingToBread(food);
        food = FillingDecorator.BEEF_MEAT.addFillingToBread(food);

        System.out.println(food.getDescription());
        System.out.println("TOTAL COST: " + food.cost());

    }
}